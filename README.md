
# Project Title

Analyseur de Billet des concerts


## Authors

- [@freemi](https://gitlab.com/freemi)
- [@fledermaus0](https://gitlab.com/fledermaus0)

## Installation


**Installation de flex/bison**

```bash
sudo apt-get install flex
sudo apt-get install bison
```

**Installation de la bibliotheque libbison-dev**
Pour la compilation avec l'option -ly

```bash
sudo apt install libbison-dev
```


## Compilation
On compile dans cette order :\
**file.lex**
```bash
lex file.lex
```
**file.yac**
```bash
yacc -d file.yac
```
La compilation des deux filchier, produit les fichiers : lex.yy.c y.tab.h y.tab.c .\
**y.tab.c** \
la commande de compilation de ce fichier est :
```bash
cc y.tab.c -lfl -ly -o file.out
```

## Hiérarchie
**le rep EXO1 :**
- Exo.1.1.lex : spécifiction reconnaissant un billet, selon la question 1.
- Exo.1.2.lex : spécifiction reconnaissant un billet, selon la question 2.
- commandeBillet.txt : fichier contenant le contenu d'un billet.\
**le rep EXO2 :**
- Exo.2.1.lex / Exo.2.1.yac  : spécifiction reconnaissant un billet, et verifie si le billet est valide, selon la question 1.
- Exo.2.2.lex / Exo.2.2.yac : spécifiction reconnaissant un billet, et affiche le nombre de place pour un dossier, selon la question 2.
- Exo.2.3.lex / Exo.2.3.yac : spécifiction reconnaissant un billet, et verifie si le billet ets valide (le billet cette fois contient une liste de place par concert).
