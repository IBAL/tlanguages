%{
#include<stdio.h>
#include <stdlib.h>
#include "y.tab.h"
char numDossier[9];
int nbrPlaceCD = 0;
int placeCount = 0;
int nbrPlace = 0;
%}
%option yylineno
codeDossier			[1-9]{8}
suiteLettre			[A-Z]+
prenomNom			{suiteLettre}(("-"{suiteLettre})*)"/"{suiteLettre}
ignore				([ ]+[\t]*)|(\t+[ ]*)
retourChariot			[\n]
nbr				[1-9][0-9]{0,1}
NumPlace			N[1-9]*

DD				(0[1-9])|([12][0-9])|(3[01])
MM				(0[1-9])|(1[0-2])
M				[1-9]
YY				[0-2][0-9]
date				({DD}"/"{MM})|({DD}"/"{M})|({DD}"/"{MM}"/"{YY})|({DD}"/"{M}"/"{YY})
HH				([01][0-9])|(2[0-3])
MN				([0-5][0-9])
heure				{HH}":"{MN}
ChiffreOuLettre		{suiteLettre}("-"[1-9]*)*
nomConcert			{ChiffreOuLettre}("-"{ChiffreOuLettre})*
codeConcert			T[1-9]{2,6}
%%
{ignore}			{}

{retourChariot}		{
					if(placeCount != nbrPlace)
					{
						return EXIT_FAILURE;
					}
					placeCount = 0;
					nbrPlace = 0; 
					return(RC);
				}
				
Dossier			{return(DOSSIER);}
places				{return(PLACES);}
{codeDossier}			{strcpy(numDossier, yytext); return(CODEDOSSIER);}
{prenomNom}			{return(PRNOM);}
{codeConcert}			{placeCount = 0; return(CODECONC);}
{nomConcert}			{return(NOMCONC);}
{date}				{return(DATE);}
{heure}			{return(HEURE);}
{nbr}				{nbrPlace = atoi(yytext); nbrPlaceCD = nbrPlaceCD + nbrPlace; return(NB);}
{NumPlace}			{nbrPlaceCD++; placeCount++; return(NUMPLACE);}
","				{return(VIRGULE);}
"["				{return(CROU);}
"]"				{return(CRFER);}
<<EOF>>			{return(FIN);}
.				{return EXIT_FAILURE;}
%%

