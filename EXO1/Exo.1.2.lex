%{
	int nbConcert;
	char prenomNom[50];
%}

%option caseless
	
codeDossier			[1-9]{8}
suiteLettre			[A-Z]+
prenomNom			{suiteLettre}(("-"{suiteLettre})*)"/"{suiteLettre}
ignore				([ ]+[\t]*)|(\t+[ ]*)
retourChariot			[\n]
nbr				[1-9][0-9]{0,1}
DD				(0[1-9])|([12][0-9])|(3[01])
MM				(0[1-9])|(1[0-2])
M				[1-9]
YY				[0-2][0-9]
date				({DD}"/"{MM})|({DD}"/"{M})|({DD}"/"{MM}"/"{YY})|({DD}"/"{M}"/"{YY})
HH				([01][0-9])|(2[0-3])
MN				([0-5][0-9])
heure				{HH}":"{MN}
ChiffreOuLettre		{suiteLettre}("-"[1-9]*)*
nomConcert			{ChiffreOuLettre}("-"{ChiffreOuLettre})*
codeConcert			T[1-9]{2,6}
%%
{ignore}			{}
{retourChariot}		{}
Dossier			{}
places				{}
{codeDossier}			{}
{prenomNom}			{strcpy(prenomNom, yytext);}
{codeConcert}			{nbConcert++;}
{nomConcert}			{}
{date}				{}
{heure}			{}
{nbr}				{}

%%
int main()
{
	yyin= fopen("commandeBillet.txt","r");
	yylex();
	printf("%s a acheté des places de %d concerts.\n", prenomNom, nbConcert);
	//fclose(yyin);
}
