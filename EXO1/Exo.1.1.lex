%{
/*#include <stdio.h>
#include <ctype.h>
int nbInt;
int nbFloat;
int nbDebut
void standChaine(char * ch)
{
	ch[0] = toupper(ch[0]);
	int i=1;
	while(ch[i] != '\0')
	{
		ch[i] = tolower(ch[i]);
		i++;
	}
}*/
%}

%option caseless
	
codeDossier			[1-9]{8}
suiteLettre			[A-Z]+
prenomNom			{suiteLettre}(("-"{suiteLettre})*)"/"{suiteLettre}
ignore				([ ]+[\t]*)|(\t+[ ]*)
retourChariot			[\n]
nbr				[1-9][0-9]{0,1}
DD				(0[1-9])|([12][0-9])|(3[01])
MM				(0[1-9])|(1[0-2])
M				[1-9]
YY				[0-2][0-9]
date				({DD}"/"{MM})|({DD}"/"{M})|({DD}"/"{MM}"/"{YY})|({DD}"/"{M}"/"{YY})
HH				([01][0-9])|(2[0-3])
MN				([0-5][0-9])
heure				{HH}":"{MN}
ChiffreOuLettre		{suiteLettre}("-"[1-9]*)*
nomConcert			{ChiffreOuLettre}("-"{ChiffreOuLettre})*
codeConcert			T[1-9]{2,6}
%%
{ignore}			{printf(" ");}
{retourChariot}		{printf(" RC\n");}
Dossier			{printf("dossier");}
places				{printf("places");}
{codeDossier}			{printf("codeDossier");}
{prenomNom}			{printf("prenomNom");}
{codeConcert}			{printf("codeConcert");}
{nomConcert}			{printf("nomConcert");}
{date}				{printf("Chaine");}
{heure}			{printf("heure");}
{nbr}				{printf("nb");}

%%
int main()
{
	yyin= fopen("commandeBillet.txt","r");
	yylex();
	printf("FinFichier\n");
	//fclose(yyin);
}
